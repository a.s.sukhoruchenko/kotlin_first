package homework7

fun main() {
    var user: User
    println("Введите логин")
    val login = readln()
    println("Введите пароль")
    val password = readln()
    println("Введите пароль еще раз")
    val repeatPassword = readln()

    try {
        user = registration(login, password, repeatPassword)
    } catch (e: WrongLoginException) {
        println(e.message)
    } catch (e: WrongPasswordException) {
        println(e.message)
    }
}

fun registration(login: String, password: String, repeatPassword: String): User {
    if (login.length > 20) {
        throw WrongLoginException("В логине не должно быть более 20 символов")
    }
    if (password.length < 10 || password != repeatPassword) {
        throw WrongPasswordException(
            "В пароле не должно быть менее 10 символов и" +
                    " пароль и подтверждение пароля должны совпадать"
        )
    }
    return User(login, password)
}

class WrongLoginException(message: String) : Exception(message)
class WrongPasswordException(message: String) : Exception(message)
class User(val login: String, val password: String)
