package homework6

import homework5.*

fun main() {
    val animalList: Array<Animal> = arrayOf(
        Lion("Лёвушка", 130, 190),
        Tiger("Тигруша", 120, 180),
        Hippo("Мотя", 165, 1600),
        Wolf("Серенький", 90, 80),
        Giraffe("Рафик", 900, 1000),
        Elephant("Ушастик", 600, 2000),
        Chimpanzee("Чичи", 50, 70),
        Gorilla("Семён", 150, 100)
    )
    val foodList: Array<String> = arrayOf(
        "beef",
        "apple",
        "mutton",
        "turkey",
        "watermelon",
        "leaves",
        "grass",
        "rabbit",
        "kiwi",
        "banana",
        "honey",
        "nuts"
    )
    feedAnimals(animalList, foodList)
}
abstract class Animal(val name: String, val height: Int, val weight: Int) {
    protected abstract val foodPreferences: Array<String>
    private var amountEaten: Int = 0
    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}
class Lion(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("chicken", "beef")
}

class Tiger(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("mutton", "turkey")
}

class Hippo(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("watermelon", "grass", "leaves")
}

class Wolf(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("rabbit", "hare")
}

class Giraffe(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("leaves", "buds")
}

class Elephant(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("roots", "apple", "kiwi")
}

class Chimpanzee(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("banana", "honey")
}

class Gorilla(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("nuts", "banana")
}

fun feedAnimals(animalList: Array<Animal>, foodList: Array<String>) {
    for (i in animalList.indices) {
        for (j in foodList.indices) {
            animalList[i].eat(foodList[j])
        }
    }
}