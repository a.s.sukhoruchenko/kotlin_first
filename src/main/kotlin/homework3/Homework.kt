package homework3

fun main() {
    val lemonade: Int = 18500
    val pina: Short = 200
    val whiskey: Byte = 50
    val fresh: Long = 3000000000
    val cola: Float = 0.5f
    val ale: Double = 0.666666667
    val authorDrink: String = "\"Что-то авторское!\""

    println("\"Заказ - \'$lemonade мл лимонада\' готов!\"")
    println("\"Заказ - \'$pina мл пина колады\' готов!\"")
    println("\"Заказ - \'$whiskey мл виски\' готов!\"")
    println("\"Заказ - \'$fresh капель фреша\' готов!\"")
    println("\"Заказ - \'$cola литра колы\' готов!\"")
    println("\"Заказ - \'$ale литра эля\' готов!\"")
    println("\"Заказ - \'$authorDrink\' готов!\"")
}