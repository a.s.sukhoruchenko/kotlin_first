package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)
fun main() {
    println("Количество овощей в корзине: ${calculateCountOfVegetables()}")
    println("Сумма покупок: ${calculateAmountOfPurchases()}")
}
fun calculateCountOfVegetables(): Int {
    var countOfVegetables = 0
    vegetableSet.forEach{
        if (userCart.containsKey(it)) {
            countOfVegetables += userCart.getValue(it)
        }
    }
    return countOfVegetables
}
fun calculateAmountOfPurchases(): Double {
    var amountOfPurchases: Double = 0.0
    userCart.forEach {
        var price = prices.getValue(it.key)
        if (discountSet.contains(it.key)) {
            price *= 1 - discountValue
        }
        amountOfPurchases += price * it.value
    }
    return amountOfPurchases
}
