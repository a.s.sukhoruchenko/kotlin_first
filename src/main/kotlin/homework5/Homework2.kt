package homework5

fun main() {
    println("Введите целое число")
    val digit = readLine()!!.toInt()
    println(reverseNumber(digit))
}

fun reverseNumber(numberForReverse: Int): Int {
    var numberForDivide: Int = numberForReverse
    var reversedNumber: Int = numberForDivide % 10
    numberForDivide /= 10
    while (numberForDivide > 10) {
        reversedNumber = reversedNumber * 10 + numberForDivide % 10
        numberForDivide /= 10
    }
    reversedNumber = reversedNumber * 10 + numberForDivide
    return reversedNumber
}