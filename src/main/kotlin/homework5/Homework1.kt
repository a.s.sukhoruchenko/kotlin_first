package homework5

fun main() {
    val lion = LionClass("Лёвушка", 130, 190)
    lion.eat("beef")
    lion.eat("apple")

    val tiger = TigerClass("Тигруша", 120, 180)
    tiger.eat("mutton")
    tiger.eat("turkey")

    val hippo = HippoClass("Мотя", 165, 1600)
    hippo.eat("watermelon")
    hippo.eat("leaves")
    hippo.eat("beef")
    hippo.eat("grass")

    val wolf = WolfClass("Серенький", 90, 80)
    wolf.eat("rabbit")

    val giraffe = GiraffeClass("Рафик", 900, 1000)
    giraffe.eat("leaves")
    giraffe.eat("turkey")

    val elephant = ElephantClass("Ушастик", 600, 2000)
    elephant.eat("apple")
    elephant.eat("kiwi")

    val chimpanzee = ChimpanzeeClass("Чичи", 50, 70)
    chimpanzee.eat("banana")
    chimpanzee.eat("honey")

    val gorilla = GorillaClass("Семён", 150, 100)
    gorilla.eat("nuts")
    gorilla.eat("honey")
}

class LionClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("chicken", "beef")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                break
            }
        }
    }
}

class TigerClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("mutton", "turkey")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}

class HippoClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("watermelon", "grass", "leaves")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}

class WolfClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("rabbit", "hare")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}

class GiraffeClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("leaves", "buds")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}

class ElephantClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("roots", "apple", "kiwi")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}

class ChimpanzeeClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("banana", "honey")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}

class GorillaClass(val name: String, val height: Int, val weight: Int) {
    val foodPreferences: Array<String> = arrayOf("nuts", "banana")
    var amountEaten: Int = 0

    fun eat(food: String) {
        for (i in foodPreferences.indices) {
            if (food == foodPreferences[i]) {
                amountEaten++
                println("$name лобит эту еду. Текущая сытость $amountEaten")
                break
            }
        }
    }
}