package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var countStudentsWithResult2: Float = 0f
    var countStudentsWithResult3: Float = 0f
    var countStudentsWithResult4: Float = 0f
    var countStudentsWithResult5: Float = 0f
    for (i in 0..marks.size-1) {
        when (marks[i]) {
            2 -> countStudentsWithResult2++
            3 -> countStudentsWithResult3++
            4 -> countStudentsWithResult4++
            5 -> countStudentsWithResult5++
        }
    }
    val percentStudentWithResult2 = countStudentsWithResult2 / marks.size * 100
    val percentStudentWithResult3 = countStudentsWithResult3 / marks.size * 100
    val percentStudentWithResult4 = countStudentsWithResult4 / marks.size * 100
    val percentStudentWithResult5 = countStudentsWithResult5 / marks.size * 100

    println("Двоечников - $percentStudentWithResult2 %")
    println("Троечников - $percentStudentWithResult3 %")
    println("Хорошистов - $percentStudentWithResult4 %")
    println("Отличников - $percentStudentWithResult5 %")
}