package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var captainCoins = 0
    var teamCoins = 0
    for (i in 0..myArray.size-1) {
        if (myArray[i] %2 == 0) {
            captainCoins += myArray[i]
        } else {
            teamCoins += myArray[i]
        }
    }
    println("Монеты капитана Джо(четные): $captainCoins")
    println("Монеты команды(нечетные): $teamCoins")
}
