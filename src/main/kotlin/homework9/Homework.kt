package homework9

const val boardSize = 4
const val emptyCell = ' '
const val shipCell = 'O'
const val hitCell = 'X'
const val missCell = '-'
val availableShips = mapOf(
    "однопалубный 1" to 1,
    "однопалубный 2" to 1,
    "двухпалубный" to 2
)

fun main() {
    println("Введите имя первого игрока")
    var playerName = readLine()!!
    val playerOne = Player(playerName)
    println("Введите имя второго игрока")
    playerName = readLine()!!
    val playerTwo = Player(playerName)
    playGame(playerOne, playerTwo)
}

fun playGame(playerOne: Player, playerTwo: Player) {
    var firstPlayer = playerOne
    var secondPlayer = playerTwo
    var isGameEnd = false
    firstPlayer.fillGameBoard()
    secondPlayer.fillGameBoard()
    while (!isGameEnd) {
        println("${firstPlayer.name}, введите координаты для выстрела")
        val coordinate = firstPlayer.enterCoordinate()
        if (secondPlayer.handleShoot(coordinate)) {
            firstPlayer.addShoot(hitCell, coordinate)
            secondPlayer.addShootOnGameBoard(hitCell, coordinate)
            if (!secondPlayer.isAlive()) {
                isGameEnd = true
            }
        } else {
            firstPlayer.addShoot(missCell, coordinate)
            val tempPlayer = firstPlayer
            firstPlayer = secondPlayer
            secondPlayer = tempPlayer
        }
    }
    println("${firstPlayer.name} победил")
}

class Coordinate(x: Int, y: Int) {
    var x: Int
        private set
    var y: Int
        private set
    val rawX: Int
        get() = this.x + 1
    val rawY: Int
        get() = this.y + 1

    init {
        this.x = x - 1
        this.y = y - 1
    }

    fun isOutOfRange(): Boolean {
        var isOutOfRange = false
        if (this.x < 0 || this.x > boardSize - 1 || this.y < 0 || this.y > boardSize - 1) {
            isOutOfRange = true
        }
        return isOutOfRange
    }
}

class WrongCoordinatesException(message: String) : Exception(message)

class WrongDirectionException(message: String) : Exception(message)

class Player(val name: String) {
    private val gameBoard = Array(boardSize) { Array(boardSize) { emptyCell } }
    private val shootBoard = Array(boardSize) { Array(boardSize) { emptyCell } }

    fun addShoot(type: Char, coordinate: Coordinate) {
        this.shootBoard[coordinate.y][coordinate.x] = type
        this.printShootBoard()
    }

    fun addShootOnGameBoard(type: Char, coordinate: Coordinate) {
        this.gameBoard[coordinate.y][coordinate.x] = type
    }

    fun isAlive(): Boolean {
        for (row in this.gameBoard) {
            for (element in row) {
                if (element == shipCell) {
                    return true
                }
            }
        }
        return false
    }

    fun handleShoot(coordinate: Coordinate): Boolean {
        return this.gameBoard[coordinate.y][coordinate.x] == shipCell
    }

    private fun printBoard(board: Array<Array<Char>>) {
        for (row in board) {
            print('|')
            for (element in row) {
                print("$element|")
            }
            println()
        }
    }

    private fun printGameBoard() {
        this.printBoard(this.gameBoard)
    }

    fun printShootBoard() {
        this.printBoard(this.shootBoard)
    }

    private fun enterNumber(numberName: Char): Int {
        println("Укажите координату $numberName")
        val number = readLine()!!.toInt()
        if (number > boardSize || number < 1) {
            throw WrongCoordinatesException("Координата должна быть от 1 до $boardSize")
        }
        return number
    }

    private fun enterDirection(): Int {
        println("Введите направление 1-вертикаль, 2-горизонталь")
        val direction: Int
        try {
            direction = readLine()!!.toInt()
            if (direction > 2 || direction < 1) {
                throw WrongDirectionException("Направление должно быть 1 или 2")
            }
        } catch (e: WrongDirectionException) {
            println(e.message)
            return this.enterDirection()
        } catch (e: Exception) {
            println("Направление должно быть целым числом")
            return this.enterDirection()
        }
        return direction
    }

    fun enterCoordinate(): Coordinate {
        val x: Int
        val y: Int
        try {
            x = this.enterNumber('x')
            y = this.enterNumber('y')
        } catch (e: WrongCoordinatesException) {
            println(e.message)
            return this.enterCoordinate()
        } catch (e: Exception) {
            println("Координата должна быть целым числом")
            return this.enterCoordinate()
        }
        return Coordinate(x, y)
    }

    private fun validateCoordinate(coordinate: Coordinate): Boolean {
        return !coordinate.isOutOfRange() && this.gameBoard[coordinate.y][coordinate.x] != shipCell
    }

    private fun placeShipOnBoard(coordinate: Coordinate, countOfCell: Int): Boolean {
        var isPlaced = true
        this.gameBoard[coordinate.y][coordinate.x] = shipCell
        if (countOfCell > 1) {
            val direction = this.enterDirection()
            for (i in 1 until countOfCell) {
                var nextCoordinate: Coordinate
                if (direction == 1) {
                    nextCoordinate = Coordinate(coordinate.rawX, coordinate.rawY + i)
                } else {
                    nextCoordinate = Coordinate(coordinate.rawX + i, coordinate.rawY)
                }
                if (this.validateCoordinate(nextCoordinate)) {
                    this.gameBoard[nextCoordinate.y][nextCoordinate.x] = shipCell
                } else {
                    isPlaced = false
                    this.gameBoard[coordinate.y][coordinate.x] = emptyCell
                }
            }
        }
        return isPlaced
    }

    fun fillGameBoard() {
        println("${this.name} заполните игровое поле")
        availableShips.forEach {
            var isValidCoordinate: Boolean = false
            while (!isValidCoordinate) {
                println("Разместите ${it.key} корабль")
                val coordinate = this.enterCoordinate()
                if (this.validateCoordinate(coordinate) && this.placeShipOnBoard(coordinate, it.value)) {
                    isValidCoordinate = true
                } else {
                    println("Клетка занята или выходит за пределы игрового поля")
                }
            }
            this.printGameBoard()
        }
    }
}